<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM users WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id ]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (isset ($_POST['nomorKTP'])  && isset($_POST['nama']) && isset($_POST['jenisKelamin']) && isset($_POST['tanggalLahir'])
  && isset($_POST['alamat'])) {
  $nomorKTP = $_POST['nomorKTP'];
  $nama = $_POST['nama'];
  $jenisKelamin = $_POST['jenisKelamin'];
  $tanggalLahir = $_POST['tanggalLahir'];
  $foto = $_FILES['foto']['name'];
  $tmp = $_FILES['foto']['tmp_name'];
  $newImage = date('dmYHis').$foto;
  $target = "images/".$foto;
  $alamat = $_POST['alamat'];
  $sql = 'UPDATE users SET nomorKTP=:nomorKTP, nama=:nama, jenisKelamin=:jenisKelamin, tanggalLahir=:tanggalLahir, foto=:foto, alamat=:alamat WHERE id=:id';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nomorKTP' => $nomorKTP, ':nama' => $nama, ':jenisKelamin' => $jenisKelamin, ':tanggalLahir' => $tanggalLahir, ':foto' => $foto, ':alamat' => $alamat, ':id' => $id])) {
    header("Location: /koperasiSISI");
    $message = 'Data berhasil diubah';
  }
}

 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Update User</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      </form><form method="post" 
      enctype="multipart/form-data">
        <div class="form-group">
          <label>Nomor KTP</label>
          <input type="text" value="<?= $person->nomorKTP; ?>" name="nomorKTP" id="nomorKTP" class="form-control">
        </div>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" value="<?= $person->nama; ?>" name="nama" id="nama" class="form-control">
        </div> 
        <div class="form-group">
          <label>Jenis Kelamin</label>
        <div class="radio">
          <input type="radio" name="jenisKelamin" value="Laki-laki"> Laki-Laki
        </div>
        <div class="radio">
          <input type="radio" name="jenisKelamin" value="Perempuan"> Perempuan
        </div>
        </div>
        <div class="form-group">
          <label>Tanggal Lahir</label>
          <input type="date" value="<?= $person->tanggalLahir; ?>" name="tanggalLahir" id="tanggalLahir" class="form-control">
        </div>
        <div class="form-group">
          <div>
          <label>Foto</label>
        <input type="file" value="<?= $person->foto; ?>" name="foto">
        </div>
        </div>
        <div class="form-group">
          <label>Alamat</label>
          <input type="text" value="<?= $person->alamat; ?>" name="alamat" id="alamat" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Ubah</button>
        </div>
      </form>
    </div>
  </div>
</div>
