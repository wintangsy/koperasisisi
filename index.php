<?php
require 'db.php';
$sql = 'SELECT *, YEAR(CURDATE()) - YEAR(tanggalLahir) AS usia FROM users';
$statement = $connection->prepare($sql);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_OBJ);
 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Daftar Anggota</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>Nomor KTP</th>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Tanggal Lahir</th>
          <th>Foto</th>
          <th>Alamat</th>
          <th>Usia</th>
          <th>Manage</th>
        </tr>
        <?php foreach($people as $person): ?>
          <tr>
            <td><?= $person->nomorKTP; ?></td>
            <td><?= $person->nama; ?></td>
            <td><?= $person->jenisKelamin; ?></td>
            <td><?= $person->tanggalLahir; ?></td>
            <td>
              <img src="images/<?php echo ['foto']; ?>" width="150" height "150"
            </td>
            <td><?= $person->alamat; ?></td>
            <td><?= $person->usia; ?></td>
            <td>
              <a href="edit.php?id=<?= $person->id ?>" class="btn btn-info">Edit</a>
              <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="delete.php?id=<?= $person->id ?>" class='btn btn-danger'>Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>
