<?php
require 'db.php';
$message = '';
if (isset ($_POST['nomorKTP'])  && isset($_POST['nama']) && isset($_POST['jenisKelamin']) && isset($_POST['tanggalLahir'])
  && isset($_POST['alamat'])) {
  $nomorKTP = $_POST['nomorKTP'];
  $nama = $_POST['nama'];
  $jenisKelamin = $_POST['jenisKelamin'];
  $tanggalLahir = $_POST['tanggalLahir'];
  $foto = $_FILES['foto']['name'];
  $tmp = $_FILES['foto']['tmp_name'];
  $newImage = date('dmYHis').$foto;
  $target = "images/".$foto;
  $alamat = $_POST['alamat'];
  $sql = 'INSERT INTO users(nomorKTP, nama, jenisKelamin, tanggalLahir,foto, alamat) 
          VALUES(:nomorKTP, :nama, :jenisKelamin, :tanggalLahir, :foto, :alamat)';
  $statement = $connection->prepare($sql);
  if ($statement->execute([':nomorKTP' => $nomorKTP, ':nama' => $nama, ':jenisKelamin' => $jenisKelamin, ':tanggalLahir' => $tanggalLahir, ':foto' => $foto, ':alamat' => $alamat])) {
    $message = 'Data berhasil dimasukkan';
  }
}


 ?>
<?php require 'header.php'; ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Pendaftaran Anggota Baru</h2>
    </div>
    <div class="card-body">
      <?php if(!empty($message)): ?>
        <div class="alert alert-success">
          <?= $message; ?>
        </div>
      <?php endif; ?>
      <form method="post" 
      enctype="multipart/form-data">
        <div class="form-group">
          <label>Nomor KTP</label>
          <input type="text" name="nomorKTP" id="nomorKTP" class="form-control">
        </div>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" name="nama" id="nama" class="form-control">
        </div> 
        <div class="form-group">
          <label>Jenis Kelamin</label>
        <div class="radio">
          <input type="radio" name="jenisKelamin" value="Laki-laki"> Laki-Laki
        </div>
        <div class="radio">
          <input type="radio" name="jenisKelamin" value="Perempuan"> Perempuan
        </div>
        </div>
        <div class="form-group">
          <label>Tanggal Lahir</label>
          <input type="date" name="tanggalLahir" id="tanggalLahir" class="form-control">
        </div>
        <div class="form-group">
          <div>
          <label>Foto</label>
        <input type="file" name="foto">
        </div>
        </div>
        <div class="form-group">
          <label>Alamat</label>
          <input type="text" name="alamat" id="alamat" class="form-control">
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-info">Daftarkan</button>
        </div>
      </form>
    </div>
  </div>
</div>
